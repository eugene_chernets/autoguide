package com.audiocarguide.data;

import android.net.Uri;
import com.google.android.gms.maps.model.LatLng;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by evgeniych on 18.01.2015.
 */
public class Tour {

    public final static int TYPE_TOUR = 0;
    public final static int TYPE_OFFER = 1;
    public final static int TYPE_ROUTE = 2;

    private String LOG_TAG = Tour.class.getSimpleName();
    private String path;
    private String tourName;
    private Country country;
    private ArrayList<TourStage> stages = new ArrayList();
    private ArrayList<LatLng> polyline = new ArrayList();
    private int type;
    private String purchaseID;
    private String downloadUrl;
    private String like;
    private String distance;
    private String duration;
    private String start;
    private String finish;
    private String route;
    private String recomendation;
    private String note;
    private String description;
    private int additionalDistance;

    public Tour (LatLng startPoint, LatLng endPoint) {
        stages.add(new TourStage("", startPoint.latitude, startPoint.longitude, Uri.parse("")));
        stages.add(new TourStage("", endPoint.latitude, endPoint.longitude, Uri.parse("")));
        type = TYPE_ROUTE;
    }

    public Tour(String dirName) {
        this.path = dirName;
        String name = "";
        double lat = 0;
        double lng = 0;
        String soundTrack = "";
        String currentTag = "";
        String parentTag = "";
        try {
            XmlPullParser xpp = prepareXpp(dirName + "/points.xml");
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        currentTag = xpp.getName();
                        if(currentTag.equals("Document"))
                            parentTag = currentTag;
                        break;
                    case XmlPullParser.TEXT:
                        if (xpp.getText().isEmpty()) {
                            break;
                        }
                        if (currentTag.equals("name")) {
                            if (parentTag.equals("Document")) {
                                this.tourName = xpp.getText().trim();
                                parentTag = "";
                            } else {
                                name = xpp.getText();
                            }
                        } else if (currentTag.equals("coordinates")) {
                            String[] strLatLng = xpp.getText().split(",");
                            lng = Double.parseDouble(strLatLng[0]);
                            lat = Double.parseDouble(strLatLng[1]);
                        } else if (currentTag.equals("ExtendedData")) {
                            soundTrack = xpp.getText().trim();
                        } else if (currentTag.equals("country")) {
                            this.country = new Country(xpp.getText().trim(), new ArrayList<Region>());
                        } else if (currentTag.equals("region")) {
                            this.country.getRegions().add(new Region(xpp.getText().trim(), true));
                        } else if (currentTag.equals("purchaseID")) {
                            this.purchaseID = xpp.getText().trim();
                        } else if (currentTag.equals("downloadUrl")) {
                            this.downloadUrl = xpp.getText().trim();
                        } else if (currentTag.equals("like")) {
                            this.like = xpp.getText().trim();
                        } else if (currentTag.equals("distance")) {
                            this.distance = xpp.getText().trim();
                        } else if (currentTag.equals("duration")) {
                            this.duration = xpp.getText().trim();
                        } else if (currentTag.equals("start")) {
                            this.start = xpp.getText().trim();
                        } else if (currentTag.equals("finish")) {
                            this.finish = xpp.getText().trim();
                        } else if (currentTag.equals("route")) {
                            this.route = xpp.getText().trim();
                        } else if (currentTag.equals("recomendation")) {
                            this.recomendation = xpp.getText().trim();
                        } else if (currentTag.equals("note")) {
                            this.note = xpp.getText().trim();
                        } else if (currentTag.equals("description")) {
                            this.description = xpp.getText().trim();
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        currentTag = "";
                        if (xpp.getName().equals("Placemark")) {
                            stages.add(new TourStage(name, lat, lng, Uri.parse(dirName + "/Sounds/" + soundTrack)));
                        }
                        break;
                }
                xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        type = stages.size() > 0 ? TYPE_TOUR : TYPE_OFFER;
    }

    @Override
    public String toString() {
        String text = "\n" + this.toString() + "\n";
        text += "path: " + path + "\n";
        text += "tourName: " + tourName + "\n";
        text += "country: " + country + "\n";
        text += "stages: " + "\n";
        for (TourStage stage : stages) {
            text += stage.toString() + "\n";
        }
        text += "polyline: " + "\n";
        for (LatLng point : polyline) {
            text += point.toString() + "\n";
        }
        return text;
    }

    public int getType() {
        return type;
    }

    public boolean isOffer() {
        return type == TYPE_OFFER;
    }

    public boolean isValid() {
        return stages.size() > 0;
    }

    private XmlPullParser prepareXpp(String fileName) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new FileInputStream(fileName), null);
        return xpp;
    }

    public String getTourName() {
        return tourName;
    }

    public ArrayList<TourStage> getStages() {
        return stages;
    }

    public ArrayList<LatLng> getRouteLineLatLngArray() {
        return polyline;
    }

    public LatLng[] getPointsLatLng() {
        LatLng[] checkPoints = new LatLng[stages.size()];
        Iterator<TourStage> iterator = stages.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            checkPoints[i++] = iterator.next().getPointLatLng();
        }
        return checkPoints;
    }

    public LatLng getOrigin() {
        return stages.get(0).getPointLatLng();
    }

    public LatLng getDestination() {
        return stages.get(stages.size() - 1).getPointLatLng();
    }

    public int getDuration() {
        int duration = 0;
        for (TourStage stage : stages)
            duration += stage.getDuration();
        return duration;
    }

    public String getDurationString() {
        if (type == TYPE_OFFER) {
            return duration;
        } else {
            int duration = 0;
            for (TourStage stage : stages)
                duration += stage.getDuration();
            int m = (duration / 60) % 60;
            int h = duration / 3600;
            return String.format("%02dh %02dm", h, m);
        }
    }

    public int getDistance() {
        int distance = 0;
        for (TourStage stage : stages)
            distance += stage.getDistance();
        return distance;
    }

    public String getDistanceString() {
        if (type == TYPE_OFFER) {
            return distance;
        } else {
            return String.format("%.2f km", (float) getDistance() / 1000);
        }
    }

    public void setAdditionalDistance(int distance) {
        this.additionalDistance = distance;
    }

    public int getAdditionalDistance(){
        return additionalDistance;
    }

    public String getPath() {
        return path;
    }

    public Country getCountry() {
        return country;
    }

    public String getOfferStart() {
        return start;
    }

    public String getOfferFinish() {
        return finish;
    }

    public String getOfferRoute() {
        return route;
    }

    public String getOfferRecomendation() {
        return recomendation;
    }

    public String getOfferNote() {
        return note;
    }

    public String getOfferDescription() {
        return description;
    }

    public String getPurchaseID() {
        return purchaseID;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }
}