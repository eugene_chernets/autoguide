package com.audiocarguide.data;

import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.*;

/**
 * Created by evgeniych on 11.02.2015.
 */
public class DirectionsProvider extends AsyncTask<Tour, Void, Tour> {

    private String TAG = DirectionsProvider.class.getSimpleName();
    private boolean needSaveResults;

    public DirectionsProvider(boolean needSaveResults) {
        super();
        this.needSaveResults = needSaveResults;
    }

    @Override
    protected Tour doInBackground(Tour... params) {
        String response = "";
        final Tour tour = params[0];
        File f = new File(tour.getPath() + "/legs.json");
        if (f.exists()) {
            response = getResponseFromFile(f);
            parseLegsArray(response, tour);//todo
        } else {
            JSONArray legsResultArray = new JSONArray();
            ArrayList<LatLng> pointsLatLngArray = new ArrayList<LatLng>(Arrays.asList(tour.getPointsLatLng()));
            for (int index = 0; index < pointsLatLngArray.size(); index += 9) {
                int lastIndex = index + 10 < pointsLatLngArray.size() ? index + 10 : pointsLatLngArray.size();
                LatLng[] portionArray = new LatLng[lastIndex - index];
                pointsLatLngArray.subList(index, lastIndex).toArray(portionArray);
                response = requestDirections(createRequestSequence(portionArray));
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String routeSearchResult = jsonResponse.getString("status");
                    Log.d(TAG, "Search Result " + routeSearchResult);
                    if ("OK".equals(routeSearchResult)) {
                        JSONArray routes = jsonResponse.getJSONArray("routes");
                        String encoded_poly;
                        JSONArray steps;
                        int distance;
                        int duration;
                        JSONArray legs = ((JSONObject) routes.get(0)).getJSONArray("legs");
                        for (int i = 0; i < legs.length(); i++) {
                            legsResultArray.put(legs.get(i));
                            distance = ((JSONObject) legs.get(i)).getJSONObject("distance").getInt("value");
                            tour.getStages().get(index + i).setDistance(distance);
                            duration = ((JSONObject) legs.get(i)).getJSONObject("duration").getInt("value");
                            tour.getStages().get(index + i).setDuration(duration);
                            steps = ((JSONObject) legs.get(i)).getJSONArray("steps");
                            for (int j = 0; j < steps.length(); j++) {
                                encoded_poly = steps.getJSONObject(j).getJSONObject("polyline").getString("points");
                                tour.getRouteLineLatLngArray().addAll(decodePoints(encoded_poly));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "JSON exception");
                    this.cancel(true);
                }
            }
            if (needSaveResults)
                saveResponse(tour.getPath(), legsResultArray);
        }
        return tour;
    }

    private void saveResponse(String path, JSONArray jsonArray){
        try {
            PrintWriter out = new PrintWriter(path + "/legs.json");
            out.println(jsonArray.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getResponseFromFile(File file) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            br.close();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    private String createRequestSequence(LatLng[] points) {
        NumberFormat numberFormat = NumberFormat.getInstance(Locale.ENGLISH);
        numberFormat.setMinimumFractionDigits(7);
        String res;
        String link = "https://maps.googleapis.com/maps/api/directions/json?";
        String params = "origin=" + numberFormat.format(points[0].latitude) + "," + numberFormat.format(points[0].longitude)
                + "&destination=" + numberFormat.format(points[points.length - 1].latitude) + ","
                + numberFormat.format(points[points.length - 1].longitude);

        res = link + params;

        if (points.length > 2) {
            String checkpoints = "&waypoints=" + numberFormat.format(points[1].latitude) + ","
                    + numberFormat.format(points[1].longitude);
            for (int i = 2; i < points.length - 1; i ++) {
                checkpoints += "|" + numberFormat.format(points[i].latitude) + "," + numberFormat.format(points[i].longitude);
            }
            res += checkpoints;
        }
        String apiKey = "&key=AIzaSyAhoEVv1cqg4tZU2oGKt8YicLcUm4Ou_rs";
        res += apiKey;
        Log.d(TAG, "created url: " + res);
        return res;
    }

    private String requestDirections(String urlStr){
        Log.d(TAG, "requesting with url " + urlStr);
        String result = "";
        try {
            URL url = new URL(urlStr);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            InputStream inputStream = httpURLConnection.getInputStream();
            httpURLConnection.connect();
            Log.d(TAG, "response " + httpURLConnection.getResponseMessage());
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader r = new BufferedReader(inputStreamReader);
            String line = r.readLine();
            while (line != null){
                result += line;
                line = r.readLine();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, result);
        return result;
    }

    public void parseLegsArray(String savedLegs, Tour tour) {
        //todo
        String encoded_poly;
        JSONArray steps;
        int distance;
        int duration;
        try {
            JSONArray legs = new JSONArray(savedLegs);
            for (int i = 0; i < legs.length(); i++) {
                distance = ((JSONObject) legs.get(i)).getJSONObject("distance").getInt("value");
                tour.getStages().get(i).setDistance(distance);
                duration = ((JSONObject) legs.get(i)).getJSONObject("duration").getInt("value");
                tour.getStages().get(i).setDuration(duration);
                steps = ((JSONObject) legs.get(i)).getJSONArray("steps");
                for (int j = 0; j < steps.length(); j++) {
                    encoded_poly = steps.getJSONObject(j).getJSONObject("polyline").getString("points");
                    tour.getRouteLineLatLngArray().addAll(decodePoints(encoded_poly));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, "JSON exception");
            this.cancel(true);
        }

    }

    private ArrayList<LatLng> decodePoints(String encoded_points){
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded_points.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded_points.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded_points.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),(((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}