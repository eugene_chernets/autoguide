package com.audiocarguide.data;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.*;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.audiocarguide.Constants;
import com.audiocarguide.R;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class ACGDataManager {

    private static final String LOG_TAG = ACGDataManager.class.getSimpleName();

    public static final String DATA_READY_ACTION = "DATA_READY_ACTION";
    public static final String DOWNLOADING_PROGRESS_UPDATE_ACTION = "DOWNLOADING_PROGRESS_UPDATE_ACTION";
    public static final String DOWNLOADING_PROGRESS_EXTRA = "DOWNLOADING_PROGRESS_UPDATE_ACTION";
//    private static final String BASE_DATA_ARCHIVE_DOWNLOAD_LINK = "http://audiocarguide.com/android_temp/test_data.zip";
    private static final String BASE_DATA_ARCHIVE_DOWNLOAD_LINK = "https://www.dropbox.com/s/fwao7d30fspdy8s/test_data.zip?raw=1";
//    private static final String BASE_DATA_ARCHIVE_DOWNLOAD_LINK = "https://www.dropbox.com/s/mfzad86x93e5wj8/base_data.zip?raw=1";
    private static final String PURCHASE_DATA_ARCHIVE_DOWNLOAD_LINK = "https://www.dropbox.com/s/twee43b20wrai9c/purchase_data.zip?raw=1";
    private static final String BASE_ARCHIVE_FILE_NAME = "test_data.zip";

    private DownloadManager downloadManager;
    private Context context;
    private LocalBroadcastManager localBroadcaster;
    private Downloading downloadingThread;
    private long downloadID;

    public ACGDataManager(Context context) {
        this.context = context;
        localBroadcaster = LocalBroadcastManager.getInstance(context);
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    private String getRoutesPath() {
        return context.getFilesDir().getPath() + Constants.ROUTES_DIRECTORY;
    }

    private String getPlacesPath() {
        return context.getFilesDir().getPath() + Constants.PLACES_DIRECTORY;
    }

    public boolean isAnyDataPresent() {
        File routesFolder = new File(getRoutesPath());
        File placesFolder = new File(getPlacesPath());
        return placesFolder.exists() && routesFolder.exists();
    }

    public boolean isEverythingDownloaded() {
        return isAnyDataPresent();//todo add logic to check if new routes need to be downloaded
    }

    public boolean downloadPurchaseData() {
        return downloadData(PURCHASE_DATA_ARCHIVE_DOWNLOAD_LINK);
    }

    public boolean downloadBaseData() {
        return downloadData(BASE_DATA_ARCHIVE_DOWNLOAD_LINK);
    }

    /*Returns true if success*/
    public boolean downloadData(String url) {
        int downloadManagerState = context.getPackageManager().getApplicationEnabledSetting(ANDROID_DWNLD_MNGR);
        if (downloadManagerState != PackageManager.COMPONENT_ENABLED_STATE_ENABLED &&
                downloadManagerState != PackageManager.COMPONENT_ENABLED_STATE_DEFAULT)
            showDownloadManagerDialog();

        checkBaseDataArchiveFileExist();

        downloadID = downloadManager.enqueue(new DownloadManager.Request(Uri.parse(url))
                .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI)
                .setAllowedOverRoaming(false)
                .setTitle(BASE_ARCHIVE_FILE_NAME)
                .setDestinationInExternalFilesDir(context, null, BASE_ARCHIVE_FILE_NAME));
        downloadingThread = new Downloading();
        downloadingThread.start();
        return true;
    }

    private void showDownloadManagerDialog() {
        final Dialog dialog = new Dialog(context, R.style.Theme_CustomDialog);
        View content = LayoutInflater.from(context).inflate(R.layout.dialog_layout, null);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(content);
        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText(context.getString(R.string.settings));
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInstalledAppDetails(context, ANDROID_DWNLD_MNGR);
                dialog.dismiss();
            }
        });
        ((TextView) content.findViewById(R.id.dialog_message)).setText(context.getString(R.string.enable_download_manager));
        content.findViewById(R.id.dialog_cancel).setVisibility(View.GONE);
        dialog.show();
    }

    private void checkBaseDataArchiveFileExist() {
        File file = new File(context.getExternalFilesDir(null) + "/" + BASE_ARCHIVE_FILE_NAME);
        if (file.exists()) {
            file.delete();
        }
    }

    public void cancelDownload() {
        if (downloadID != 0) {
            downloadManager.remove(downloadID);
            downloadingThread.cancel();
            checkBaseDataArchiveFileExist();
        }
    }

    private void unpackZip(String sourcePath, String file) {
        String destinationPath = context.getFilesDir().getPath();
        InputStream is;
        ZipInputStream zis;
        try {
            String filename;
            is = new FileInputStream(sourcePath + "/" + file);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();
                if (ze.isDirectory()) {
                    File fmd = new File(destinationPath + "/" + filename);
                    fmd.mkdirs();
                    continue;
                }
                FileOutputStream fileOutputStream = new FileOutputStream(destinationPath + "/" + filename);
                while ((count = zis.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, count);
                }
                fileOutputStream.close();
                zis.closeEntry();
            }
            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(null, e.toString());
        }
        Log.d(LOG_TAG, "unzipping finished");
    }

    public void onPause() {
        context.unregisterReceiver(connectivityReceiver);
        context.unregisterReceiver(onDownloadingComplete);
        if (downloadingThread != null)
            downloadingThread.cancel();
    }

    public void onResume() {
        context.registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        context.registerReceiver(onDownloadingComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        startDownloadThread();
    }

    private void startDownloadThread() {
        Cursor cursor = downloadManager.query(new DownloadManager.Query().setFilterById(downloadID));
        if (cursor.moveToFirst()) {
            int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            if (status == DownloadManager.STATUS_RUNNING || status == DownloadManager.STATUS_PENDING) {
                downloadingThread = new Downloading();
                downloadingThread.start();
            } else if (status == DownloadManager.STATUS_SUCCESSFUL) {
                Unzipping unzipping = new Unzipping();
                unzipping.start();
            }
        }
        cursor.close();
    }

    class Downloading extends Thread {
        private boolean downloading = true;

        public void cancel() {
            downloading = false;
            Log.d(LOG_TAG, "downloading thread cancel received");
        }

        @Override
        public void run() {
            Log.d(LOG_TAG, "downloading thread started");
            int downloadedBytes;
            int totalBytes;
            DownloadManager.Query query;
            while (downloading) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                query = new DownloadManager.Query();
                query.setFilterById(downloadID);
                Cursor cursor = downloadManager.query(query);
                if (cursor.moveToFirst()) {
                    int state = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    Log.d(LOG_TAG, "downloading status " + state);
                    if (state == DownloadManager.STATUS_FAILED) {
                        int reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                        Log.d(LOG_TAG, "fail reason " + reason);
                        if (reason == DownloadManager.ERROR_FILE_ALREADY_EXISTS) {
                            Unzipping unzipping = new Unzipping();
                            unzipping.start();
                        } else {
                            cancelDownload();
                        }
                        downloading = false;
                        break;
                    }
                    if (state == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                        break;
                    }
                    downloadedBytes = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    totalBytes = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    float progress = ((float)downloadedBytes / (float)totalBytes);
                    Log.d(LOG_TAG,"downloading progress_image " + progress);
                    Intent intent = new Intent(DOWNLOADING_PROGRESS_UPDATE_ACTION);
                    intent.putExtra(DOWNLOADING_PROGRESS_EXTRA, progress > 0.99f ? 0.99f : progress);
                    localBroadcaster.sendBroadcast(intent);
                }
                cursor.close();
            }
        }
    }

    class Unzipping extends Thread {

        @Override
        public void run() {
            Log.d(LOG_TAG, "unzipping started");
            unpackZip(context.getExternalFilesDir(null).getPath(), BASE_ARCHIVE_FILE_NAME);
            Intent intent = new Intent(DATA_READY_ACTION);
            localBroadcaster.sendBroadcast(intent);
        }
    }

    private static final String SCHEME = "package";
    private static final String APP_PKG_NAME_21 = "com.android.settings.ApplicationPkgName";
    private static final String APP_PKG_NAME_22 = "pkg";
    private static final String APP_DETAILS_PACKAGE_NAME = "com.android.settings";
    private static final String APP_DETAILS_CLASS_NAME = "com.android.settings.InstalledAppDetails";
    private static final String ANDROID_DWNLD_MNGR = "com.android.providers.downloads";

    public static void showInstalledAppDetails(Context context, String packageName) {
        Intent intent = new Intent();
        final int apiLevel = Build.VERSION.SDK_INT;
        if (apiLevel >= 9) { // above 2.3
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts(SCHEME, packageName, null);
            intent.setData(uri);
        } else { // below 2.3
            final String appPkgName = (apiLevel == 8 ? APP_PKG_NAME_22 : APP_PKG_NAME_21);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setClassName(APP_DETAILS_PACKAGE_NAME, APP_DETAILS_CLASS_NAME);
            intent.putExtra(appPkgName, packageName);
        }
        context.startActivity(intent);
    }

    private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo == null || netInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                    cancelDownload();
                }
            }
        }
    };

    private BroadcastReceiver onDownloadingComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Cursor cursor = downloadManager.query(new DownloadManager.Query().setFilterById(downloadID));
            if (cursor.moveToFirst()) {
                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) ==
                        DownloadManager.STATUS_SUCCESSFUL) {
                    Unzipping unzipping = new Unzipping();
                    unzipping.start();
                }
            }
            cursor.close();
            Log.d(LOG_TAG, intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0) + " ACTION_DOWNLOAD_COMPLETE received");
        }
    };
}