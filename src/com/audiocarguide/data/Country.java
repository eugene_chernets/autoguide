package com.audiocarguide.data;

import java.util.ArrayList;

/**
 * Created by evgeniych on 23.06.2015.
 */
public class Country {

    private String name;
    private ArrayList<Region> regions;

    public Country(String name, ArrayList<Region> regions) {
        this.name = name;
        this.regions = regions;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    @Override
    public boolean equals(Object object) {
        return this.name.equals(((Country)object).getName());
    }

}

