package com.audiocarguide.data;

import android.location.Location;
import android.net.Uri;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by evgeniych on 18.01.2015.
 */
public class TourStage {

    private String name;
    private LatLng pointLatLng;
    private Uri soundtrack;
    private int duration;
    private int distance;

    public TourStage(String name, double lat, double lng, Uri sound) {
        this.name = name;
        this.pointLatLng = new LatLng(lat, lng);
        this.soundtrack = sound;
    }

    @Override
    public String toString() {
        String text = "\n" + this.toString() + "\n";
        text += "name: " + name + "\n";
        text += "pointLatLng: " + pointLatLng + "\n";
        text += "soundtrack: " + soundtrack + "\n";
        text += "duration: " + duration + "\n";
        text += "distance: " + distance + "\n";
        return text;
    }

    public LatLng getPointLatLng() {
        return pointLatLng;
    }

    public String getName() {
        return name;
    }

    public Uri getSoundTrackUri() {
        return soundtrack;
    }

    public Location getLocation() {
        Location location = new Location("TourPoint");
        location.setLatitude(pointLatLng.latitude);
        location.setLongitude(pointLatLng.longitude);
        return location;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public String getDurationString() {
        int m = (duration / 60) % 60 ;
        int h = duration / 3600;
        return String.format("%02dh %02dm", h, m);
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }

    public String getDistanceString() {
        return String.format("%.1f", (float)distance / 1000);
    }


}
