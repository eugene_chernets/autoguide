package com.audiocarguide.data;

/**
 * Created by evgeniych on 23.06.2015.
 */
public class Region {

    private String name;
    private boolean isActive;

    public Region(String name, boolean isActive) {
        this.name = name;
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    public boolean equals(Object object) {
        return this.name.equals(((Region)object).getName());
    }
}
