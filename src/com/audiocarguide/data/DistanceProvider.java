package com.audiocarguide.data;

import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by evgeniych on 11.02.2015.
 */
public class DistanceProvider extends AsyncTask<LatLng, Void, Integer> {

    private String TAG = DistanceProvider.class.getSimpleName();

    @Override
    protected Integer doInBackground(LatLng... params) {
        int distance = 0;
        String response = "";
        LatLng origin = params[0];
        LatLng destination = params[1];
                response = requestDistance(createRequestSequence(origin, destination));
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String routeSearchResult = jsonResponse.getString("status");
                    Log.d(TAG, "Search Result " + routeSearchResult);
                    if ("OK".equals(routeSearchResult)) {
                        JSONArray rows = jsonResponse.getJSONArray("rows");
                        JSONArray elements = ((JSONObject) rows.get(0)).getJSONArray("elements");
                        distance = ((JSONObject) elements.get(0)).getJSONObject("distance").getInt("value");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "JSON exception");
                }
        return distance;
    }

    private String createRequestSequence(LatLng origin, LatLng destination) {
        NumberFormat numberFormat = NumberFormat.getInstance(Locale.ENGLISH);
        numberFormat.setMinimumFractionDigits(7);
        String res;
        String link = "https://maps.googleapis.com/maps/api/distancematrix/json?";
        String params = "origins=" + numberFormat.format(origin.latitude) + "," + numberFormat.format(origin.longitude)
                + "&destinations=" + numberFormat.format(destination.latitude) + "," + numberFormat.format(destination.longitude);

        res = link + params;
        Log.d(TAG, "created url: " + res);
        return res;
    }

    private String requestDistance(String urlStr){
        Log.d(TAG, "requesting with url " + urlStr);
        String result = "";
        try {
            URL url = new URL(urlStr);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            InputStream inputStream = httpURLConnection.getInputStream();
            httpURLConnection.connect();
            Log.d(TAG, "response " + httpURLConnection.getResponseMessage());
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader r = new BufferedReader(inputStreamReader);
            String line = r.readLine();
            while (line != null){
                result += line;
                line = r.readLine();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, result);
        return result;
    }
}