package com.audiocarguide.data;

import android.content.Context;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by evgeniych on 18.01.2015.
 */
public class Place {
    private Context context;
    private String path;
    private String name;
    private LatLng pointLatLng;
    private Places.PlaceType type;
    private String description;
    private String picsFolderPath;

    public Place(Context context, String path, String name, double lat, double lng, Places.PlaceType type,
                 String description, String picsFolderPath) {
        this.context = context;
        this.path = path;
        this.type = type;
        this.name = name;
        this.pointLatLng = new LatLng(lat, lng);
        this.description = description;
        this.picsFolderPath = picsFolderPath;
    }

    @Override
    public String toString() {
        String text = "\n" + this.toString() + "\n";
        text += "path: " + path + "\n";
        text += "name: " + name + "\n";
        text += "pointLatLng: " + pointLatLng + "\n";
        text += "type: " + type + "\n";
        text += "description: " + description + "\n";
        text += "picsFolderPath: " + picsFolderPath + "\n";
        return text;
    }

    public LatLng getPlaceLatLng() {
        return pointLatLng;
    }

    public String getName() {
        return name;
    }

    public Places.PlaceType getType() {
        return type;
    }

    public String getPicsFolderPath() {
        return path + "/Images/" + picsFolderPath;
    }

    public  String getDescription() {
        return description;
    }
}
