package com.audiocarguide.data;

import android.content.Context;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by evgeniych on 18.01.2015.
 */
public class Places {

    private String path;
    private ArrayList<Place> places = new ArrayList<Place>();

    public Places(Context context, String dirName) {
        this.path = dirName;
        PlaceType type = PlaceType.UNKNOWN;
        double lat = 0;
        double lng = 0;
        String name = "";
        String description = "";
        String pictures = "";
        String currentTag = "";

        try {
            XmlPullParser xpp = prepareXpp(dirName + "/places.xml");
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        currentTag = xpp.getName();
                        break;
                    case XmlPullParser.TEXT:
                        if (xpp.getText().isEmpty()) {
                            break;
                        }
                        if (currentTag.equals("name")) {
                                name = xpp.getText();
                        } else if (currentTag.equals("coordinates")) {
                            String[] strLatLng = xpp.getText().split(",");
                            lng = Double.parseDouble(strLatLng[0]);
                            lat = Double.parseDouble(strLatLng[1]);
                        } else if (currentTag.equals("description")) {
                            description = xpp.getText().trim();
                        } else if (currentTag.equals("images")) {
                            pictures = xpp.getText().trim();
                        } else if (currentTag.equals("type")) {
                            type = PlaceType.parse(xpp.getText().trim());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        currentTag = "";
                        if (xpp.getName().equals("Placemark")) {
                            places.add(new Place(context, path, name, lat, lng, type, description, pictures));
                        }
                        break;
                }
                xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Place> getPlaces() {
        return places;
    }

    public Place getPlaceByMarkerName(String markerName) {
        Place place = null;
        for (int i = 0; i < places.size(); i++) {
            if (places.get(i).getName().equals(markerName))
                place = places.get(i);
        }
        return place;
    }

    private XmlPullParser prepareXpp(String fileName) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new FileInputStream(fileName), null);
        return xpp;
    }

    @Override
    public String toString() {
        String text = "\n" + this.toString() + "\n";
        text += "path: " + path + "\n";
        text += "places: " + "\n";
        for (Place place : places) {
            text += place.toString() + "\n";
        }
        return text;
    }

    public enum PlaceType {
        SHOP,
        RESTAURANT,
        SOUVENIR,
        HOTEL,
        PLACE,
        UNKNOWN;

        public static PlaceType parse(String text) {
            if ("shop".equals(text)) {
                return SHOP;
            } else if ("restaurant".equals(text)) {
                return RESTAURANT;
            } else if ("souvenir".equals(text)) {
                return SOUVENIR;
            } else if ("hotel".equals(text)) {
                return HOTEL;
            } else if ("place".equals(text)) {
                return PLACE;
            } else {
                return UNKNOWN;
            }
        }
    }

}
