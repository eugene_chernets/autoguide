package com.audiocarguide;

/**
 * Created by evgeniych on 08.01.2015.
 */
public interface Constants {

    public static final String SHARED_PREFERENCES_KEY = "com.audiocarguide.preferences";
    public static final String SHARED_PREFERENCES_KEY_PLACES_ENABLED = "com.audiocarguide.preferences.places";

    public static final String ROUTES_DIRECTORY = "/routes";
    public static final String PLACES_DIRECTORY = "/places";

    public static final String ACTIVE_FRAGMENT_POSITION = "ACTIVE_FRAGMENT_POSITION";

    public static final String SELECTED_TOUR = "SELECTED_TOUR";

    public static final int TOURS_FRAGMENT_INDEX = 0;
//    public static final int ABOUT_FRAGMENT_INDEX = 1;
    public static final int TUTORIAL_FRAGMENT_INDEX = 1;
    public static final int FEEDBACK_FRAGMENT_INDEX = 2;
    public static final int SETTINGS_FRAGMENT_INDEX = 3;


}
