package com.audiocarguide.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CheckBox;
import com.audiocarguide.R;

/**
 * Created by evgeniych on 23.06.2015.
 */
public class ACGCheckBox extends CheckBox {
    public ACGCheckBox(Context context) {
        super(context);
    }

    public ACGCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public ACGCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.custom_text_view);
        CharSequence font = attributes.getString(R.styleable.custom_text_view_font);
        if (font != null) {
            super.setTypeface(Typefaces.get(context, font.toString()));
        }
    }
}