package com.audiocarguide.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import com.audiocarguide.R;

public class ACGTextView extends TextView {

    public ACGTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    public ACGTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public ACGTextView(Context context) {
        super(context);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.custom_text_view);
        CharSequence font = attributes.getString(R.styleable.custom_text_view_font);
        if (font != null) {
            super.setTypeface(Typefaces.get(context, font.toString()));
        }
    }
}
