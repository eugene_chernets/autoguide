package com.audiocarguide.custom;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SlidingDrawer;
import com.audiocarguide.R;

/**
 * Created by evgeniych on 02.03.2015.
 */
public class ACGSlidingDrawer extends SlidingDrawer {
    private ViewGroup mHandleLayout;
    private final Rect mHitRect = new Rect();

    public ACGSlidingDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ACGSlidingDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View handle = getHandle();
        if (handle instanceof ViewGroup) {
            mHandleLayout = (ViewGroup) handle;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (mHandleLayout != null) {
            int handleClickX = (int)(event.getX() - mHandleLayout.getLeft());
            int handleClickY = (int)(event.getY() - mHandleLayout.getTop());

            Rect hitRect = mHitRect;

            View childView = mHandleLayout.findViewById(R.id.play_pause_button);
            childView.getHitRect(hitRect);
            if (hitRect.contains(handleClickX, handleClickY)) {
                return false;
            }
        }

        return super.onInterceptTouchEvent(event);
    }
}