package com.audiocarguide.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Created by evgeniych on 28.04.2015.
 */
public class ACGUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final String TAG = ACGUncaughtExceptionHandler.class.getSimpleName();
    private Context context;

    public ACGUncaughtExceptionHandler(Context context) {
        this.context = context;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        String text = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (int i = 0; i < stackTrace.length; i++)
            text += stackTrace[i].toString() + "\n";
        Log.e(TAG, ex.toString());
        Log.e(TAG, text);
        Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "evgen.chernets@mail.ru", null));
        i.putExtra(Intent.EXTRA_SUBJECT, "AudioCarGuide Crash: " + ex.toString());
        i.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(i, "Send email..."));
        ((Activity)context).finish();
    }
}
