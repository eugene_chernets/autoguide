package com.audiocarguide.utils;

/**
 * Created by evgeniych on 24.06.2015.
 */
public interface FilterListener {
    void addFiltered(String name);
    void removeFiltered(String name);
}
