package com.audiocarguide;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.audiocarguide.adapters.ACGDrawerListAdapter;
import com.audiocarguide.fragments.SettingsFragment;
import com.audiocarguide.fragments.ToursListFragment;
import com.audiocarguide.fragments.TutorialFragment;


/**
 * Created by evgeniych on 17.12.2014.
 */
public class MainActivity extends FragmentActivity implements AdapterView.OnItemClickListener {

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ACGDrawerListAdapter drawerListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Thread.setDefaultUncaughtExceptionHandler(new AGUncaughtExceptionHandler(this));

        setContentView(R.layout.main_activity);

        int position = getIntent().getIntExtra(Constants.ACTIVE_FRAGMENT_POSITION, 0);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.drawer_list);

        drawerListAdapter = new ACGDrawerListAdapter(this, position);
        drawerList.setAdapter(drawerListAdapter);
        drawerList.setOnItemClickListener(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, getFragmentByPosition(position))
                .commit();
    }

    @Override
    public void onBackPressed() {
        View v = findViewById(R.id.detail_view);
        if (v != null && v.getVisibility() == View.VISIBLE) {
            return;
        }
        super.onBackPressed();
    }

    public void onSideMenuButtonClick(View view) {
            drawerLayout.openDrawer(Gravity.LEFT);
    }

    public void onStartRouteClick(View view) {
        Intent intent = new Intent(this, RouteActivity.class);
        intent.putExtra(Constants.SELECTED_TOUR, (String)view.getTag());
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == Constants.FEEDBACK_FRAGMENT_INDEX) {
            Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "evgen.chernets@gmail.com", null));
            i.putExtra(Intent.EXTRA_SUBJECT, "AudioCarGuide feedback");
            startActivity(Intent.createChooser(i, "Send email..."));
        } else {
            drawerListAdapter.setIconSelected(position);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, getFragmentByPosition(position))
                    .commit();
        }
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    private Fragment getFragmentByPosition(int position) {
        Fragment fragment = new ToursListFragment();
        switch(position) {
            case Constants.TOURS_FRAGMENT_INDEX:
                fragment = new ToursListFragment();
                break;
            case Constants.TUTORIAL_FRAGMENT_INDEX:
                fragment = new TutorialFragment();
                break;
            case Constants.SETTINGS_FRAGMENT_INDEX:
                fragment = new SettingsFragment();
                break;
        }
        return fragment;
    }
}
