package com.audiocarguide;

import android.app.Activity;
import android.app.Dialog;
import android.content.*;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.*;
import com.android.vending.billing.IInAppBillingService;
import com.audiocarguide.data.ACGDataManager;
import com.audiocarguide.data.DirectionsProvider;
import com.audiocarguide.data.Tour;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by evgeniych on 17.12.2014.
 */
public class StartActivity extends Activity {
    private static final String TAG = StartActivity.class.getSimpleName();
    private static final String KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmsUKCQ/Xv/cv9MwAKKuGgqOwqXS3LOQxKZGMucfLwrUIOX/chU5B6icj2HjGbJd3xTvM8Sc6hgFrgdE4VMy2o+2J8m22W/AJlH9dXlRIFXaEGT9bUUjmMDV04a1e9CH+tqZtGEVneSuyKQ1Vqyf0CNZu67NWUVGPiVQJRCBkpW5PDR4HT42Gcrj28M1ogenX00I91UZfEc9sv52BS84KDtJq7LO1TZvjs5XMIf0WN5mD20eTLV/RBndxfi1G4ZAojXD3JH30uOOYBNUjGr7Af3D4egvMv5e4ULmURn9/FLyQP53nlZPAt8P7lQnQOxzciPIjYCwhkSvddWpkiJDXpwIDAQAB";

    private LinearLayout rootViewBg, rootViewBgBlured, hiddenButtonsContainer, dots1, dots2, dots3, dots4;
    private boolean buttonsVisible = false;
    private ACGDataManager dataManager;
    private RelativeLayout loadingScreen;
    private AnimationDrawable loadingAnimation;
    private TextView loadingProgressMessage;
    private LocalBroadcastManager localBroadcaster;
    private Dialog dialog;
    private final ArrayList<Tour> tours = new ArrayList<>();
    private final ArrayList<String> toursPurchaseIDs = new ArrayList<>();
    private IInAppBillingService iabService;

    ServiceConnection iabServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            iabService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            iabService = IInAppBillingService.Stub.asInterface(service);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Thread.setDefaultUncaughtExceptionHandler(new AGUncaughtExceptionHandler(this));

        setContentView(R.layout.start_activity);
        localBroadcaster = LocalBroadcastManager.getInstance(this);
        rootViewBg = (LinearLayout) findViewById(R.id.start_activity_bg);
        rootViewBgBlured = (LinearLayout) findViewById(R.id.start_activity_bg_blured);
        hiddenButtonsContainer = (LinearLayout) findViewById(R.id.hidden_buttons_container);
        dots1 = (LinearLayout) findViewById(R.id.dots1);
        dots2 = (LinearLayout) findViewById(R.id.dots2);
        dots3 = (LinearLayout) findViewById(R.id.dots3);
        dots4 = (LinearLayout) findViewById(R.id.dots4);
        loadingScreen = (RelativeLayout) findViewById(R.id.loading_screen_layout);
        loadingAnimation = (AnimationDrawable) ((ImageView) findViewById(R.id.loading_animation)).getDrawable();
        loadingProgressMessage = (TextView) findViewById(R.id.loading_screen_message);

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, iabServiceConnection, Context.BIND_AUTO_CREATE);

        showLoadingView();
        dataManager = new ACGDataManager(this);
        if (isInternetConnectionAlive())
            dataManager.downloadPurchaseData();

//        if (!dataManager.isEverythingDownloaded()) {//todo remove this condition and leave only isEverythingDownloaded() check
//            showLoadingView();
//            if (isInternetConnectionAlive())
//                dataManager.downloadData();
//        } else {
//            Log.d(TAG, "base data present");
//        }

        rootViewBgBlured.startAnimation(createAlphaAnimation(1, 0, 10, 0));
        hiddenButtonsContainer.startAnimation(createAlphaAnimation(1, 0, 10, 0));
        dots1.startAnimation(createAlphaAnimation(1, 0, 10, 0));
        dots2.startAnimation(createAlphaAnimation(1, 0, 10, 0));
        dots3.startAnimation(createAlphaAnimation(1, 0, 10, 0));
        dots4.startAnimation(createAlphaAnimation(1, 0, 10, 0));
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInternetConnectionAlive();
        dataManager.onResume();
        localBroadcaster.registerReceiver(onDownloadingProgress, new IntentFilter(ACGDataManager.DOWNLOADING_PROGRESS_UPDATE_ACTION));
        localBroadcaster.registerReceiver(onDataReady, new IntentFilter(ACGDataManager.DATA_READY_ACTION));
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        dataManager.onPause();
        localBroadcaster.unregisterReceiver(onDownloadingProgress);
        localBroadcaster.unregisterReceiver(onDataReady);
        unregisterReceiver(connectivityReceiver);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (iabService != null) {
            unbindService(iabServiceConnection);
        }
    }

    private AlphaAnimation createAlphaAnimation(float startAlpha, float endAlpha, long duration, long delay) {
        AlphaAnimation animation = new AlphaAnimation(startAlpha, endAlpha);
        animation.setDuration(duration);
        animation.setStartOffset(delay);
        animation.setFillAfter(true);
        return animation;
    }

    public void onStartClick(View view) {
        if (buttonsVisible) {
            rootViewBg.startAnimation(createAlphaAnimation(0, 1, 2000, 0));
            rootViewBgBlured.startAnimation(createAlphaAnimation(1, 0, 2000, 0));
            hiddenButtonsContainer.startAnimation(createAlphaAnimation(1, 0, 1000, 0));
            dots4.startAnimation(createAlphaAnimation(1, 0, 200, 800));
            dots3.startAnimation(createAlphaAnimation(1, 0, 200, 1000));
            dots2.startAnimation(createAlphaAnimation(1, 0, 200, 1200));
            dots1.startAnimation(createAlphaAnimation(1, 0, 200, 1400));
            buttonsVisible = false;
        } else {
            rootViewBg.startAnimation(createAlphaAnimation(1, 0, 2000, 0));
            rootViewBgBlured.startAnimation(createAlphaAnimation(0, 1, 2000, 0));
            dots1.startAnimation(createAlphaAnimation(0, 1, 200, 0));
            dots2.startAnimation(createAlphaAnimation(0, 1, 200, 200));
            dots3.startAnimation(createAlphaAnimation(0, 1, 200, 400));
            dots4.startAnimation(createAlphaAnimation(0, 1, 200, 600));
            hiddenButtonsContainer.startAnimation(createAlphaAnimation(0, 1, 1000, 600));
            buttonsVisible = true;
        }
    }

    public void onLeftButtonClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.ACTIVE_FRAGMENT_POSITION, Constants.TUTORIAL_FRAGMENT_INDEX);
        startActivity(intent);
    }

    public void onRightButtonClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.ACTIVE_FRAGMENT_POSITION, Constants.TOURS_FRAGMENT_INDEX);
        startActivity(intent);
    }

    private boolean isInternetConnectionAlive() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting() ||
                activeNetwork.getType() != ConnectivityManager.TYPE_WIFI) {
            showInternetConnectionDialog();
            return false;
        } else {
            return true;
        }
    }

    private void showInternetConnectionDialog() {
        if (dialog != null) {
            dialog = new Dialog(this, R.style.Theme_CustomDialog);
            View content = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(content);
            ((TextView) content.findViewById(R.id.dialog_message)).setText(getString(R.string.connect_to_wifi));
            Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
            buttonOk.setText(getResources().getString(R.string.settings));
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    dialog.dismiss();
                }
            });
            Button buttonCancel = (Button) content.findViewById(R.id.dialog_cancel);
            buttonCancel.setText(getResources().getString(R.string.cancel));
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    onBackPressed();
                }
            });
            dialog.show();
        }
    }

    private void showLoadingView() {
        loadingScreen.setVisibility(View.VISIBLE);
        loadingAnimation.start();
        loadingProgressMessage.setText(getString(R.string.loading, 0));
    }

    private void getArrayOfToursPurchaseID() {
        File routesDir = new File(getFilesDir().getPath() + "/routes");
        boolean exists = routesDir.exists();
        boolean isDirectory = routesDir.isDirectory();
        if (exists && isDirectory)
            for (String fileName : routesDir.list()) {
                try {
                    DirectionsProvider directionsProvider = new DirectionsProvider(true);
                    final Tour tour = new Tour(routesDir.getPath() + "/" + fileName);
                    directionsProvider.execute(tour);
                    tours.add(directionsProvider.get());
                    toursPurchaseIDs.add(tours.get(tours.size() - 1).getPurchaseID());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
    }

    private void checkAllPurchasesDownloaded() {
        if (isInternetConnectionAlive() && toursPurchaseIDs.size() == 0) {
            getArrayOfToursPurchaseID();
        }
        final Bundle queryOwnedItems = new Bundle();
        queryOwnedItems.putStringArrayList("ITEM_ID_LIST", toursPurchaseIDs);

        AsyncTask getOwnedItemsTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Bundle ownedItems = iabService.getPurchases(3, getPackageName(), "inapp", null);
                    if (ownedItems.getInt("RESPONSE_CODE") == 0) {
                        ArrayList<String> ownedIDs = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                        for (int i = 0; i < ownedIDs.size(); i++) {
                            searchAndCheckTourDownloaded(ownedIDs.get(i));
                        }
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        getOwnedItemsTask.execute();
    }

    private void searchAndCheckTourDownloaded(String id) {
        for (Tour tour : tours) {
            if (tour.getPurchaseID().equals(id) && tour.isOffer())
                dataManager.downloadData(tour.getDownloadUrl());
        }
    }

    private BroadcastReceiver onDownloadingProgress = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            float progress = intent.getFloatExtra(ACGDataManager.DOWNLOADING_PROGRESS_EXTRA, 0);
            loadingProgressMessage.setText(getString(R.string.loading, (int) (progress * 100)));
        }
    };

    private BroadcastReceiver onDataReady = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            checkAllPurchasesDownloaded();



//            loadingAnimation.stop();//todo move hiding loading screen to appropriate line
//            loadingScreen.setVisibility(View.GONE);
        }
    };

    private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    dataManager.downloadPurchaseData();
                }
            }
        }
    };
}