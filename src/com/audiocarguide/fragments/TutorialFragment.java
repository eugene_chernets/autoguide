package com.audiocarguide.fragments;


import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.audiocarguide.R;

/**
 * Created by evgeniych on 29.03.2015.
 */
public class TutorialFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View tutorialView = inflater.inflate(R.layout.tutorial_fragment, null);
        ViewPager pager = (ViewPager)tutorialView.findViewById(R.id.tutorial_pager);
        final TypedArray pages = getActivity().getResources().obtainTypedArray(R.array.tutorial_pages);
        pager.setAdapter(new FragmentPagerAdapter(getActivity().getSupportFragmentManager()) {
            @Override
            public Fragment getItem(final int i) {
                return new Fragment() {
                    @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                        View pageView = inflater.inflate(R.layout.viewpager_page, null);
                        ImageView image = (ImageView)pageView.findViewById(R.id.pager_image);
                        image.setImageDrawable(pages.getDrawable(i));
                        return pageView;
                    }
                };
            }

            @Override
            public int getCount() {
                return pages.length();
            }
        });

        final LinearLayout pagerIndicator = (LinearLayout)tutorialView.findViewById(R.id.pager_indicator);
        for (int i = 0; i < pages.length(); i++) {
            ImageView indicator = new ImageView(getActivity());
            indicator.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            if (i == 0)
                indicator.setImageDrawable(getResources().getDrawable(R.drawable.dot_active_photo));
            else
                indicator.setImageDrawable(getResources().getDrawable(R.drawable.dot_next_photo));
            pagerIndicator.addView(indicator);
        }

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int current = 0;
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                ((ImageView)pagerIndicator.getChildAt(current))
                        .setImageDrawable(getResources().getDrawable(R.drawable.dot_next_photo));
                ((ImageView)pagerIndicator.getChildAt(i))
                        .setImageDrawable(getResources().getDrawable(R.drawable.dot_active_photo));
                current = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        return tutorialView;
    }
}
