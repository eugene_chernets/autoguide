package com.audiocarguide.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import com.audiocarguide.Constants;
import com.audiocarguide.R;

/**
 * Created by evgeniych on 06.01.2015.
 */
public class SettingsFragment extends Fragment  {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.settings_fragment, null);
        final SharedPreferences preferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, getActivity().MODE_PRIVATE);
        ToggleButton placesSwitcher = (ToggleButton)view.findViewById(R.id.settings_places_switcher);
        placesSwitcher.setChecked(preferences.getBoolean(Constants.SHARED_PREFERENCES_KEY_PLACES_ENABLED, false));
        placesSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Constants.SHARED_PREFERENCES_KEY_PLACES_ENABLED, isChecked);
                editor.commit();
            }
        });
        return view;
    }
}
