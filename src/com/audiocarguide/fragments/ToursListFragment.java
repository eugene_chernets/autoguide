package com.audiocarguide.fragments;

import android.app.Dialog;
import android.content.*;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.audiocarguide.Constants;
import com.audiocarguide.R;
import com.audiocarguide.adapters.ACGOfferDetailsListAdapter;
import com.audiocarguide.adapters.ACGToursListAdapter;
import com.audiocarguide.adapters.ACGFilterListAdapter;
import com.audiocarguide.data.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by evgeniych on 06.01.2015.
 */
public class ToursListFragment extends Fragment implements OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ExpandableListView.OnGroupClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener,
        View.OnClickListener, GoogleMap.OnMapClickListener /*, GpsStatus.Listener*/ {

    private final String TAG = ToursListFragment.class.getSimpleName();
    private final ArrayList<Tour> tours = new ArrayList<Tour>();
    private volatile GoogleMap googleMap;
    private Places places;
    private MapView mapView;
    private GoogleApiClient googleApiClient;
    private ExpandableListView toursList;
    private ACGToursListAdapter toursListAdapter;
    private LocationManager locationManager;
    private ViewPager pager;
    private RelativeLayout detailView;
    private SlidingDrawer toursListDrawer;
    private DrawerLayout filterDrawerLayout;
    private ExpandableListView filterList;
    private ACGFilterListAdapter filterListAdapter;
    private View collapseMapButton;
    private Bitmap[] pagerPictures;
    private Dialog dialog;
    private boolean markerInfoWindowShown = false;
    private boolean placesEnabled = false;


    public ToursListFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tours_list_fragment, null);

        getLocationService();

        mapView = (MapView) view.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        toursListDrawer = (SlidingDrawer) view.findViewById(R.id.roues_list_drawer);
        toursListDrawer.close();
        toursList = (ExpandableListView) view.findViewById(R.id.routes_expandable_list);
        toursList.setOnGroupClickListener(this);

        filterDrawerLayout = (DrawerLayout) view.findViewById(R.id.filter_drawer_layout);
        filterList = (ExpandableListView) view.findViewById(R.id.filter_list);

        if (internetConnectionAlive()) {
            getTours();
        }

        SharedPreferences preferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, getActivity().MODE_PRIVATE);
        placesEnabled = preferences.getBoolean(Constants.SHARED_PREFERENCES_KEY_PLACES_ENABLED, false);
        if (placesEnabled) {
            places = new Places(getActivity(), getActivity().getFilesDir().getPath() + Constants.PLACES_DIRECTORY);
        }

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        detailView = (RelativeLayout) view.findViewById(R.id.detail_view);
        pager = (ViewPager) detailView.findViewById(R.id.details_viewpager);

        view.findViewById(R.id.filter_button).setOnClickListener(this);
        view.findViewById(R.id.details_close).setOnClickListener(this);
        view.findViewById(R.id.details_enlarge).setOnClickListener(this);

        collapseMapButton = view.findViewById(R.id.collapse_map_button);
        collapseMapButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, new LocationRequest()
                    .setInterval(30000)
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY), this);
        } else {
            googleApiClient.connect();
        }

        if (internetConnectionAlive() && tours.size() == 0) {
            getTours();
        }
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            showGpsProviderDialog();

        getActivity().registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        mapView.onPause();

        googleMap.setOnMapLoadedCallback(null);

        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);

        getActivity().unregisterReceiver(connectivityReceiver);

        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity());
        this.googleMap = googleMap;
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
        googleMap.setOnMapClickListener(this);

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View view = getActivity().getLayoutInflater().inflate(R.layout.marker_info_window, null);
                ((TextView) view.findViewById(R.id.info_text)).setText(marker.getTitle());
                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (tours != null && tours.size() > 0) {
                    Tour tour = tours.get(toursListAdapter.getSelectedGroup());
                    if (!tour.isOffer()) {
                        drawMarkers(tour.getStages());
                        drawRouteLine(new PolylineOptions().addAll(tour.getRouteLineLatLngArray()));
                        drawPlacesMarkers();
                    }
                }
            }
        });
    }

    private synchronized void getLocationService() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private void showGpsProviderDialog() {

        final Dialog dialog = new Dialog(getActivity(), R.style.Theme_CustomDialog);
        View content = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_layout, null);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(content);

        ((TextView) content.findViewById(R.id.dialog_message)).setText("GPS is disabled in your device. Would you like to enable it?");

        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText("Settings");
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
                dialog.dismiss();
            }
        });

        Button buttonCancel = (Button) content.findViewById(R.id.dialog_cancel);
        buttonCancel.setText("Cancel");
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private boolean internetConnectionAlive() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting()) {
            showInternetConnectionDialog();
            return false;
        } else {
            return true;
        }
    }

    private void showInternetConnectionDialog() {
        if (dialog != null && dialog.isShowing())
            return;
        dialog = new Dialog(getActivity(), R.style.Theme_CustomDialog);
        View content = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_layout, null);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(content);
        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText(getResources().getString(R.string.settings));
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                dialog.dismiss();
            }
        });
        ((TextView) content.findViewById(R.id.dialog_message)).setText(getActivity().getString(R.string.connect_to_internet));
        content.findViewById(R.id.dialog_cancel).setVisibility(View.GONE);
        dialog.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, new LocationRequest()
                .setInterval(60000)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY), this);
        String res = lastLocation == null ? " unknown" : lastLocation.toString();
        Log.d(TAG, "lastLocation:" + res);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        requestDistances(location);
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        toursListAdapter.setGroupSelected(groupPosition);
        googleMap.clear();
        if (!tours.get(groupPosition).isOffer()) {
            drawMarkers(tours.get(groupPosition).getStages());
            drawRouteLine(new PolylineOptions().addAll(tours.get(groupPosition).getRouteLineLatLngArray()));
            drawPlacesMarkers();
        }
        return true;
    }

    private void showOfferDetailsDialog(int tourIndex) {
        Tour tour = tours.get(tourIndex);

        View content = LayoutInflater.from(getActivity()).inflate(R.layout.offer_details_dialog, null);
        ((TextView)content.findViewById(R.id.offer_name)).setText(tour.getTourName());
        ((TextView)content.findViewById(R.id.offer_duration)).setText(tour.getDurationString());
        ((TextView)content.findViewById(R.id.offer_start)).setText(tour.getOfferStart());
        ((TextView)content.findViewById(R.id.offer_finish)).setText(tour.getOfferFinish());

        String[] detailsList = {tour.getOfferRoute(), tour.getOfferRecomendation(), tour.getOfferNote(), tour.getOfferDescription()};
        ExpandableListView offerDetailsList = ((ExpandableListView) content.findViewById(R.id.offer_details_list));
        offerDetailsList.setAdapter(new ACGOfferDetailsListAdapter(getActivity(), offerDetailsList, detailsList));
        content.findViewById(R.id.offer_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        content.findViewById(R.id.offer_buy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo
                dialog.dismiss();
            }
        });

        dialog = new Dialog(getActivity(), R.style.Theme_CustomDialog);
        dialog.setContentView(content);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void drawMarkers(ArrayList<TourStage> points) {
        TypedArray pins = getResources().obtainTypedArray(R.array.tour_points_pins);
        for (int i = 0; i < points.size(); i++) {
            googleMap.addMarker(new MarkerOptions()
                    .position(points.get(i).getPointLatLng())
                    .title(points.get(i).getName())
                    .infoWindowAnchor(1, 0)
                    .icon(BitmapDescriptorFactory.fromResource(pins.getResourceId(i < pins.length() ? i : 0, 0))));
        }
    }

    private void drawPlacesMarkers() {
        if (!placesEnabled) return;
        Place place;
        TypedArray pins = getResources().obtainTypedArray(R.array.places_pins);
        for (int i = 0; i < places.getPlaces().size(); i++) {
            place = places.getPlaces().get(i);
            int pinIndex = place.getType().ordinal();
            googleMap.addMarker(new MarkerOptions()
                    .position(place.getPlaceLatLng())
                    .title(place.getName())
                    .infoWindowAnchor(1, 0)
                    .icon(BitmapDescriptorFactory.fromResource(pins.getResourceId(pinIndex < pins.length() ? pinIndex : 0, 0))));
        }
    }

    private void drawRouteLine(PolylineOptions polylineOptions) {
        polylineOptions.width(10);
        polylineOptions.color(getResources().getColor(R.color.route_line));
        googleMap.addPolyline(polylineOptions);
        zoomAndCenter(polylineOptions.getPoints());
    }

    private void requestDistances(Location location) {
        for (Tour tour : tours) {
            DistanceProvider distanceProvider = new DistanceProvider() {
                @Override
                protected void onPostExecute(Integer integer) {
                    refreshList();
                }
            };
            distanceProvider.execute(new LatLng(location.getLatitude(), location.getLongitude()), tour.getOrigin());
            try {
                tour.setAdditionalDistance(distanceProvider.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private void getTours() {
        File routesDir = new File(getActivity().getFilesDir().getPath() + "/routes");
        boolean exists = routesDir.exists();
        boolean isDirectory = routesDir.isDirectory();
        if (exists && isDirectory)
            for (String fileName : routesDir.list()) {
                try {
                    DirectionsProvider directionsProvider = new DirectionsProvider(true);
                    final Tour tour = new Tour(routesDir.getPath() + "/" + fileName);
                    directionsProvider.execute(tour);
                    tours.add(directionsProvider.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        toursListAdapter = new ACGToursListAdapter(getActivity(), toursList, this, tours);
        toursList.setAdapter(toursListAdapter);
        toursListDrawer.animateOpen();

        filterListAdapter = new ACGFilterListAdapter(getActivity(), tours, toursListAdapter);
        filterList.setAdapter(filterListAdapter);
    }

    private void refreshList() {
        toursListAdapter.notifyDataSetChanged();
        toursList.refreshDrawableState();
        if (googleMap != null && tours.size() > 0) {
            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "refreshList");
                            Tour tour = tours.get(toursListAdapter.getSelectedGroup());
                            if (!tour.isOffer()) {
                                drawMarkers(tour.getStages());
                                drawRouteLine(new PolylineOptions().addAll(tour.getRouteLineLatLngArray()));
                                drawPlacesMarkers();
                            }
                        }
                    });
                }
            });
        }
    }

    private void zoomAndCenter(List<LatLng> points) {
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        for (int i = 0; i < points.size(); i++)
            b.include(points.get(i));
        LatLngBounds bounds = b.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 40);
        googleMap.animateCamera(cameraUpdate);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        markerInfoWindowShown = true;
        marker.showInfoWindow();
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Place place = places.getPlaceByMarkerName(marker.getTitle());
        if (place != null) {
            File picsDir = new File(place.getPicsFolderPath());
            if (picsDir.list() == null) return;
            String filePath;
            BitmapFactory.Options options = new BitmapFactory.Options();
            detailView.setVisibility(View.VISIBLE);
            ((TextView) detailView.findViewById(R.id.details_description_title)).setText(place.getName());
            ((TextView) detailView.findViewById(R.id.details_description_text)).setText(place.getDescription());
            pagerPictures = new Bitmap[picsDir.list().length];
            for (int i = 0; i < pagerPictures.length; i++) {
                filePath = picsDir.getPath() + "/" + picsDir.list()[i];
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(filePath, options);
                options.inSampleSize = options.outWidth / getActivity().getWindow().getDecorView().getWidth();
                options.inJustDecodeBounds = false;
                pagerPictures[i] = BitmapFactory.decodeFile(filePath, options);
            }

            pager.setAdapter(new FragmentPagerAdapter(getActivity().getSupportFragmentManager()) {
                @Override
                public Fragment getItem(final int i) {
                    return new Fragment() {
                        @Override
                        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                            View pageView = inflater.inflate(R.layout.viewpager_page, null);
                            ImageView image = (ImageView) pageView.findViewById(R.id.pager_image);
                            image.setImageBitmap(pagerPictures[i]);
                            return pageView;
                        }
                    };
                }

                @Override
                public int getCount() {
                    return pagerPictures.length;
                }
            });

            final LinearLayout pagerIndicator = (LinearLayout) detailView.findViewById(R.id.details_pager_indicator);
            pagerIndicator.removeAllViews();
            for (int i = 0; i < pagerPictures.length; i++) {
                ImageView indicator = new ImageView(getActivity());
                indicator.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                if (i == 0)
                    indicator.setImageDrawable(getResources().getDrawable(R.drawable.dot_active_photo));
                else
                    indicator.setImageDrawable(getResources().getDrawable(R.drawable.dot_next_photo));
                pagerIndicator.addView(indicator);
            }

            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                private int current = 0;

                @Override
                public void onPageScrolled(int i, float v, int i1) {
                }

                @Override
                public void onPageSelected(int i) {
                    ((ImageView) pagerIndicator.getChildAt(current))
                            .setImageDrawable(getResources().getDrawable(R.drawable.dot_next_photo));
                    ((ImageView) pagerIndicator.getChildAt(i))
                            .setImageDrawable(getResources().getDrawable(R.drawable.dot_active_photo));
                    current = i;
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.collapse_map_button:
                toursListDrawer.animateOpen();
                view.setVisibility(View.GONE);
                break;
            case R.id.filter_button:
                filterDrawerLayout.openDrawer(Gravity.RIGHT);
            case R.id.details_close:
                detailView.setVisibility(View.GONE);
                pagerPictures = null;
                pager.setAdapter(null);
                pager.destroyDrawingCache();
                System.gc();
                break;
            case R.id.details_enlarge:
                View description = detailView.findViewById(R.id.details_description_panel);
                if (description.getVisibility() == View.VISIBLE) {
                    description.setVisibility(View.GONE);
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.btn_details_collapse));
                } else {
                    description.setVisibility(View.VISIBLE);
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.btn_details_enlarge));
                }
                break;
            case R.id.btn_action:
                showOfferDetailsDialog((int)view.getTag());
                break;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (markerInfoWindowShown) {
            markerInfoWindowShown = false;
        } else {
            if (toursListAdapter != null && toursListAdapter.getGroupCount() > 0 && toursListDrawer.isOpened()) {
                toursListDrawer.animateClose();
                collapseMapButton.setVisibility(View.VISIBLE);
            }
        }
    }

    public void hideTourListDrawer() {
        collapseMapButton.setVisibility(View.GONE);
        if (toursListDrawer.isOpened()) {
            toursListDrawer.animateClose();
        }
    }

    public void showTourListDrawer() {
        toursListDrawer.animateOpen();
    }

    private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                    if (tours.size() == 0)
                        getTours();
                }
            }
        }
    };
}