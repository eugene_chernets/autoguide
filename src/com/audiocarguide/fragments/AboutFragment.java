package com.audiocarguide.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.audiocarguide.R;

/**
 * Created by evgeniych on 06.01.2015.
 */
public class AboutFragment extends Fragment  {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.about_fragment, null);

        return view;
    }



}
