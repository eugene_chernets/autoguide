package com.audiocarguide;

import android.app.Dialog;
import android.content.*;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.audiocarguide.adapters.ACGListAdapter;
import com.audiocarguide.data.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by evgeniych on 17.12.2014.
 */
public class RouteActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GpsStatus.Listener,
        CompoundButton.OnCheckedChangeListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private final String TAG = RouteActivity.class.getSimpleName();

    private Tour tour;
    private Places places;
    private MapView mapView;
    private LocationManager locationManager;
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    private Location currentLocation;
    private Location previousLocation;
    private ListView listView;
    private Marker myLocationMarker;
    private int distanceLeft = 0;
    private int additionalDistanceLeft = 0;
    private int durationLeft = 0;
    private int additionalDurationLeft = 0;
    private int currentPolyLinePointIndex = -1;
    private int currentTourStageIndex = -1;
    private int pauseTime = 0;
    private int outOfLine = 0;
    private long outOfRouteTimestamp = 0;
    private float zoom = 15;
    private boolean isAdditionalDirectionsNeeded = true;
    private boolean pausedInLifecycle = false;
    private boolean introFinished = false;
    private boolean placesEnabled = false;
    private ArrayList<LatLng> polylinePoints = new ArrayList<LatLng>();
    private Polyline routeLine;
    private MediaPlayer mediaPlayer;
    private ToggleButton playPauseButton;
    private RelativeLayout infoPanel;
    private RelativeLayout detailView;
    private Bitmap[] pagerPictures;
    private ViewPager pager;
    private RelativeLayout loadingScreen;
    private AnimationDrawable loadingAnimation;
    private LinearLayout handlePointsContainer;
    private Dialog internetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                String text = "";
                StackTraceElement[] stackTrace = ex.getStackTrace();
                for (int i = 0; i < stackTrace.length; i++)
                    text += stackTrace[i].toString() + "\n";

                text += "tour: " + tour + "\n\n";
                text += "places: " + places + "\n\n";
                text += "mapView: " + mapView + "\n\n";
                text += "locationManager: " + locationManager + "\n\n";
                text += "googleMap: " + googleMap + "\n\n";
                text += "googleApiClient: " + googleApiClient+ "\n\n";
                text += "currentLocation: " + currentLocation + "\n\n";
                text += "previousLocation: " + previousLocation + "\n\n";
                text += "listView: " + listView + "\n\n";
                text += "distanceLeft: " + distanceLeft + "\n\n";
                text += "additionalDistanceLeft: " + additionalDistanceLeft + "\n\n";
                text += "durationLeft: " + durationLeft + "\n\n";
                text += "additionalDurationLeft: " + additionalDurationLeft + "\n\n";
                text += "currentPolyLinePointIndex: " + currentPolyLinePointIndex + "\n\n";
                text += "currentTourStageIndex: " + currentTourStageIndex + "\n\n";
                text += "pauseTime: " + pauseTime + "\n\n";
                text += "outOfLine: " + outOfLine + "\n\n";
                text += "outOfRouteTimestamp: " + outOfRouteTimestamp + "\n\n";
                text += "zoom: " + zoom + "\n\n";
                text += "isAdditionalDirectionsNeeded: " + isAdditionalDirectionsNeeded + "\n\n";
                text += "pausedInLifecycle: " + pausedInLifecycle + "\n\n";
                text += "introFinished: " + introFinished + "\n\n";
                text += "placesEnabled: " + placesEnabled + "\n\n";
                text += "polylinePoints: " + "\n";
                for (LatLng point : polylinePoints) { text += point.toString() + "\n"; }
                text += "routeLine: " + routeLine + "\n\n";
                text += "mediaPlayer: " + mediaPlayer + "\n\n";
                text += "playPauseButton: " + playPauseButton + "\n\n";
                text += "infoPanel: " + infoPanel + "\n\n";
                text += "detailView: " + detailView + "\n\n";
                text += "pagerPictures: " + pagerPictures + "\n\n";
                text += "pager: " + pager + "\n\n";
                text += "loadingScreen: " + loadingScreen + "\n\n";
                text += "loadingAnimation: " + loadingAnimation + "\n\n";
                text += "handlePointsContainer: " + handlePointsContainer + "\n\n";
                text += "internetDialog: " + internetDialog + "\n\n";

                Log.e(TAG, ex.toString());
                Log.e(TAG, text);

                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "evgen.chernets@mail.ru", null));
                i.putExtra(Intent.EXTRA_SUBJECT, "AudioCarGuide Crash: " + ex.toString());
                i.putExtra(Intent.EXTRA_TEXT, text);
                startActivity(Intent.createChooser(i, "Send email..."));
//                finish();
            }
        });

        setContentView(R.layout.route_activity);

        loadingScreen = (RelativeLayout) findViewById(R.id.loading_screen_layout);
        loadingAnimation = (AnimationDrawable) ((ImageView) findViewById(R.id.loading_animation)).getDrawable();
        ((TextView) findViewById(R.id.loading_screen_message)).setText(getString(R.string.please_wait, 0));

        showLoadingScreen();

        infoPanel = (RelativeLayout) findViewById(R.id.info_panel);
        detailView = (RelativeLayout) findViewById(R.id.detail_view);
        pager = (ViewPager) detailView.findViewById(R.id.details_viewpager);

        findViewById(R.id.info_button).setOnClickListener(this);
        findViewById(R.id.details_close).setOnClickListener(this);
        findViewById(R.id.details_enlarge).setOnClickListener(this);

        playPauseButton = (ToggleButton) findViewById(R.id.play_pause_button);
        playPauseButton.setOnCheckedChangeListener(this);
        playPauseButton.setEnabled(false);

        getLocationService();
        mapView = (MapView) findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        String path = getIntent().getStringExtra(Constants.SELECTED_TOUR);
        if (path != null) {
            try {
                tour = new DirectionsProvider(true).execute(new Tour(path)).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        } else {
            showDialog(getResources().getString(R.string.internal_data_error_message), true, false);
        }

        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, MODE_PRIVATE);
        placesEnabled = preferences.getBoolean(Constants.SHARED_PREFERENCES_KEY_PLACES_ENABLED, false);
        if (placesEnabled) {
            places = new Places(this, getFilesDir().getPath() + Constants.PLACES_DIRECTORY);
        }

        polylinePoints.addAll(tour.getRouteLineLatLngArray());

        listView = (ListView) findViewById(R.id.routes_expandable_list);
        listView.setAdapter(new ACGListAdapter(this, tour.getStages()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentTourStageIndex = position;
                Toast.makeText(RouteActivity.this, "Current stage set to " + position, Toast.LENGTH_SHORT).show();
            }
        });

        handlePointsContainer = (LinearLayout) findViewById(R.id.handle_route_points);
        for (int i = 0; i < tour.getStages().size(); i++) {
            TextView textView = (TextView) LayoutInflater.from(this).inflate(R.layout.handle_tour_point, null);
            textView.setText((i + 1) + "");
            handlePointsContainer.addView(textView);
        }

        ((TextView) findViewById(R.id.handle_route_title)).setText(tour.getTourName());
        distanceLeft = tour.getDistance();
        durationLeft = tour.getDuration();
        setHandleDistance();
        setHandleDuration();

        setProgressView(0);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        mapView.onResume();

        if (mediaPlayer != null && tour != null) {
            try {
                if (currentTourStageIndex >= 0) {
                    mediaPlayer.setDataSource(getApplicationContext(),
                            tour.getStages().get(currentTourStageIndex).getSoundTrackUri());
                    mediaPlayer.prepare();
                    mediaPlayer.seekTo(pauseTime);
                    if (pausedInLifecycle) {
                        mediaPlayer.start();
                        pausedInLifecycle = false;
                    }
                } else {
                    mediaPlayer.setDataSource(this, Uri.parse(tour.getPath() + "/Sounds/intro.mp3"));
                    mediaPlayer.prepare();
                    if (pausedInLifecycle) {
                        mediaPlayer.seekTo(pauseTime);
                        pausedInLifecycle = false;
                    }
                    if (!introFinished) {
                        mediaPlayer.start();
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.setOnCompletionListener(null);
                                introFinished = true;
                            }
                        });
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "playback resumed in lifecycle");
        }

        internetConnectionAlive();

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            showGpsProviderDialog();

        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, new LocationRequest()
                    .setInterval(1000)
                    .setFastestInterval(200)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY), this);
        } else {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        mapView.onPause();

        if (mediaPlayer != null && !playPauseButton.isChecked()) {
            mediaPlayer.pause();
            pauseTime = mediaPlayer.getCurrentPosition();
            pausedInLifecycle = true;
            Log.d(TAG, "playback paused in lifecycle");
        }

        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        unregisterReceiver(connectivityReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        showDialog(getResources().getString(R.string.confirm_stop_routing), true, true);
    }

    @Override
    protected void onStop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View view = getLayoutInflater().inflate(R.layout.marker_info_window, null);
                ((TextView) view.findViewById(R.id.info_text)).setText(marker.getTitle());
                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        this.googleMap = googleMap;

        if (currentLocation != null)
            setMyMarkerTo(currentLocation);

        hideLoadingScreen();
        drawPlacesMarkers();
        drawMarkers(tour.getStages());
        drawRouteLine(new PolylineOptions().addAll(polylinePoints));
    }

    private synchronized void getLocationService() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, new LocationRequest()
                .setInterval(1000)
                .setFastestInterval(200)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY), this);

        currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        previousLocation = currentLocation;
        if (isAdditionalDirectionsNeeded && currentLocation != null)
            checkAdditionalDirections(currentLocation);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showDialog(getResources().getString(R.string.google_services_error_message), true, false);
    }

    @Override
    public void onGpsStatusChanged(int event) {
        switch (event) {
            case GpsStatus.GPS_EVENT_STOPPED:
                showGpsProviderDialog();
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "location changed" + location.toString());
        previousLocation = currentLocation != null ? currentLocation : location;
        currentLocation = location;

        if (isAdditionalDirectionsNeeded)
            checkAdditionalDirections(currentLocation);

        if (currentPolyLinePointIndex < polylinePoints.size() - 1 &&
                location.distanceTo(getPolyLinePointLocation(currentPolyLinePointIndex + 1)) < 100) {

            currentPolyLinePointIndex++;

            if (currentPolyLinePointIndex == polylinePoints.size() - 1) {
                // stop routing
                LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.setOnCompletionListener(null);
                        showDialog(getResources().getString(R.string.route_finished_message), true, false);
                    }
                });
                setProgressView(100);
                return;
            }

            setProgressView(currentPolyLinePointIndex * 100 / polylinePoints.size());

            if (currentPolyLinePointIndex > 0) {
                decreaseDistances((int) getPolyLinePointLocation(currentPolyLinePointIndex).distanceTo(
                        getPolyLinePointLocation(currentPolyLinePointIndex - 1)));
                setHandleDistance();
            }
        }
        setMyMarkerTo(location);
        checkOutOfRoute(location);

        if (currentTourStageIndex < tour.getStages().size() - 1)
            checkNextTourPointReached(location);
    }

    public void onCancelButtonClick(View v) {
        showDialog(getResources().getString(R.string.confirm_stop_routing), true, true);
    }

    private void showDialog(String message, final boolean stopActivity, final boolean cancelable) {
        final Dialog dialog = new Dialog(this, R.style.Theme_CustomDialog);
        View content = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setCancelable(cancelable);
        dialog.setContentView(content);

        ((TextView) content.findViewById(R.id.dialog_message)).setText(message);

        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText(R.string.ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (stopActivity)
                    finish();
            }
        });
        if (cancelable) {
            Button buttonCancel = (Button) content.findViewById(R.id.dialog_cancel);
            buttonCancel.setText(R.string.cancel);
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } else {
            content.findViewById(R.id.dialog_cancel).setVisibility(View.GONE);
        }
        dialog.show();
    }

    private void showGpsProviderDialog() {

        final Dialog gpsDialog = new Dialog(this, R.style.Theme_CustomDialog);
        View content = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        gpsDialog.setCanceledOnTouchOutside(false);
        gpsDialog.setCancelable(false);
        gpsDialog.setContentView(content);

        ((TextView) content.findViewById(R.id.dialog_message)).setText(getString(R.string.connect_to_gps));

        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText(R.string.ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
                gpsDialog.dismiss();
            }
        });
        content.findViewById(R.id.dialog_cancel).setVisibility(View.GONE);
        gpsDialog.show();
    }

    private boolean internetConnectionAlive() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting()) {
            showInternetConnectionDialog();
            return false;
        } else {
            return true;
        }
    }

    private void showInternetConnectionDialog() {
        if (internetDialog != null && internetDialog.isShowing())
            return;
        internetDialog = new Dialog(this, R.style.Theme_CustomDialog);
        View content = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        internetDialog.setCanceledOnTouchOutside(false);
        internetDialog.setCancelable(false);
        internetDialog.setContentView(content);
        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText(getResources().getString(R.string.settings));
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                internetDialog.dismiss();
            }
        });
        ((TextView) content.findViewById(R.id.dialog_message)).setText(getString(R.string.connect_to_internet));
        content.findViewById(R.id.dialog_cancel).setVisibility(View.GONE);
        internetDialog.show();
    }

    private void drawMarkers(ArrayList<TourStage> points) {

        TypedArray pins = getResources().obtainTypedArray(R.array.tour_points_pins);
        for (int i = 0; i < points.size(); i++) {
            googleMap.addMarker(new MarkerOptions()
                    .position(points.get(i).getPointLatLng())
                    .title(points.get(i).getName())
                    .infoWindowAnchor(1, 0)
                    .icon(BitmapDescriptorFactory.fromResource(pins.getResourceId(i, 0))));
        }
    }

    private void drawRouteLine(PolylineOptions polylineOptions) {
        if (googleMap != null) {
            if (routeLine != null)
                routeLine.setVisible(false);
            polylineOptions.width(12);
            polylineOptions.color(getResources().getColor(R.color.route_line));
            routeLine = googleMap.addPolyline(polylineOptions);
        }
    }

    private void drawPlacesMarkers() {
        if (!placesEnabled) return;
        Place place;
        TypedArray pins = getResources().obtainTypedArray(R.array.places_pins);
        for (int i = 0; i < places.getPlaces().size(); i++) {
            place = places.getPlaces().get(i);
            int pinIndex = place.getType().ordinal();
            googleMap.addMarker(new MarkerOptions()
                    .position(place.getPlaceLatLng())
                    .title(place.getName())
                    .infoWindowAnchor(1, 0)
                    .icon(BitmapDescriptorFactory.fromResource(pins.getResourceId(pinIndex < pins.length() ? pinIndex : 0, 0))));
        }
    }

    private void setMyMarkerTo(Location location) {
        LatLng locationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (googleMap != null) {
            if (myLocationMarker == null) {
                myLocationMarker = googleMap.addMarker(new MarkerOptions()
                        .position(locationLatLng)
                        .anchor(0.5f, 0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_current_location))
                        .title("You are here"));
            } else {
                myLocationMarker.setPosition(locationLatLng);
            }
        }
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(locationLatLng, zoom, 0,
                location.bearingTo(previousLocation) - 180f)));
    }

    private Location getPolyLinePointLocation(int index) {
        int correctIndex = index < polylinePoints.size() ? index : polylinePoints.size() - 1;
        Location nextLocation = new Location(TAG);
        nextLocation.setLatitude(polylinePoints.get(correctIndex).latitude);
        nextLocation.setLongitude(polylinePoints.get(correctIndex).longitude);
        return nextLocation;
    }

    private Location latLngToLocation(LatLng latLng) {
        Location location = new Location(TAG);
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    private void checkNextTourPointReached(Location location) {
        TourStage point = tour.getStages().get(currentTourStageIndex + 1);
        if (location.distanceTo(point.getLocation()) < 100) {
            currentTourStageIndex++;
            if (currentTourStageIndex == 0) {
                distanceLeft = tour.getDistance();
                durationLeft = tour.getDuration();
                setHandleDistance();
                setHandleDuration();
            } else {
                decreaseDurations(tour.getStages().get(currentTourStageIndex - 1).getDuration());
                setHandleDuration();
            }

            if (currentTourStageIndex > 0) {
                TextView view = (TextView) handlePointsContainer.getChildAt(currentTourStageIndex - 1);
                view.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_grey));
                view.setTextColor(0xffbbbbbb);
            }
            if (currentTourStageIndex < handlePointsContainer.getChildCount()) {
                TextView view = (TextView) handlePointsContainer.getChildAt(currentTourStageIndex);
                view.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_green));
            }

            playPauseButton.setEnabled(true);
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                mediaPlayer.reset();
                try {
                    mediaPlayer.setDataSource(getApplicationContext(),
                            tour.getStages().get(currentTourStageIndex).getSoundTrackUri());
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (playPauseButton.isChecked())
                if (mediaPlayer != null)
                    mediaPlayer.pause();
        }
    }

    private void checkAdditionalDirections(Location location) {
        TourStage point = tour.getStages().get(0);
        if (location.distanceTo(point.getLocation()) > 500) {
            requestAdditionalDirection(location);
        } else {
            isAdditionalDirectionsNeeded = false;
        }
    }

    private void requestAdditionalDirection(Location location) {
        if (!internetConnectionAlive() || currentTourStageIndex == tour.getStages().size() - 1) return;
        showLoadingScreen();
        LatLng originLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        LatLng destinationLatLng = tour.getPointsLatLng()[currentTourStageIndex < 0 ? 0 : currentTourStageIndex + 1];
        Tour additionalTour = new Tour(originLatLng, destinationLatLng);
        DirectionsProvider directionsProvider = new DirectionsProvider(false);
        directionsProvider.execute(additionalTour);
        try {
            Tour result = directionsProvider.get();
            if (result.getRouteLineLatLngArray().size() == 0)
                return;
            Location destination = latLngToLocation(destinationLatLng);
            while (destination.distanceTo(latLngToLocation(polylinePoints.get(0))) > 5)
                polylinePoints.remove(0);
            polylinePoints.addAll(0, result.getRouteLineLatLngArray());
            drawRouteLine(new PolylineOptions().addAll(polylinePoints));
            currentPolyLinePointIndex = 0;
            isAdditionalDirectionsNeeded = false;
            additionalDistanceLeft = additionalTour.getDistance();
            additionalDurationLeft = additionalTour.getDuration();
            setHandleDistance();
            setHandleDuration();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        hideLoadingScreen();
    }

    private void checkOutOfRoute(Location currentLocation) {
        if (currentPolyLinePointIndex < 0 || polylinePoints.size() == currentPolyLinePointIndex + 1) return;

        LatLng lineStartLatLng = polylinePoints.get(currentPolyLinePointIndex);
        LatLng lineEndLatLng = polylinePoints.get(currentPolyLinePointIndex + 1);

        Location lineStartLocation = new Location("lineStartLocation");
        lineStartLocation.setLatitude(lineStartLatLng.latitude);
        lineStartLocation.setLongitude(lineStartLatLng.longitude);

        Location lineEndLocation = new Location("lineEndLocation");
        lineEndLocation.setLatitude(lineEndLatLng.latitude);
        lineEndLocation.setLongitude(lineEndLatLng.longitude);

        int startToEndDistance = (int) lineStartLocation.distanceTo(lineEndLocation);
        int currentFromStartDistance = (int) currentLocation.distanceTo(lineStartLocation);
        int currentToEndDistance = (int) currentLocation.distanceTo(lineEndLocation);

        outOfLine = Math.abs(startToEndDistance - currentFromStartDistance - currentToEndDistance);

        Log.d(TAG, "checkOutOfRoute outOfLine " + outOfLine);

        if (outOfLine > 200) {
            //out of route
            long currentTime = System.currentTimeMillis();
            if (outOfRouteTimestamp == 0) {
                outOfRouteTimestamp = currentTime;
            } else if (currentTime - outOfRouteTimestamp > 15000) {
                Toast.makeText(this, "Out of route", Toast.LENGTH_SHORT).show();
                requestAdditionalDirection(currentLocation);
                outOfRouteTimestamp = 0;
            }
        } else {
            outOfRouteTimestamp = 0;
        }
    }

    public void showLoadingScreen() {
        loadingScreen.setVisibility(View.VISIBLE);
        loadingAnimation.start();
    }

    public void hideLoadingScreen() {
        loadingAnimation.stop();
        loadingScreen.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.info_button:
                if (infoPanel.getVisibility() == View.VISIBLE)
                    infoPanel.setVisibility(View.GONE);
                else
                    infoPanel.setVisibility(View.VISIBLE);
                break;
            case R.id.details_close:
                detailView.setVisibility(View.GONE);
                pagerPictures = null;
                pager.setAdapter(null);
                pager.destroyDrawingCache();
                System.gc();
                break;
            case R.id.details_enlarge:
                View description = detailView.findViewById(R.id.details_description_panel);
                if (description.getVisibility() == View.VISIBLE) {
                    description.setVisibility(View.GONE);
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.btn_details_collapse));
                } else {
                    description.setVisibility(View.VISIBLE);
                    ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.btn_details_enlarge));
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            mediaPlayer.pause();
            pauseTime = mediaPlayer.getCurrentPosition();
        } else {
            mediaPlayer.start();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Place place = places.getPlaceByMarkerName(marker.getTitle());
        if (place != null) {
            File picsDir = new File(place.getPicsFolderPath());
            if (picsDir.list() == null) return;
            String filePath;
            BitmapFactory.Options options = new BitmapFactory.Options();
            detailView.setVisibility(View.VISIBLE);
            ((TextView) detailView.findViewById(R.id.details_description_title)).setText(place.getName());
            ((TextView) detailView.findViewById(R.id.details_description_text)).setText(place.getDescription());
            pagerPictures = new Bitmap[picsDir.list().length];
            for (int i = 0; i < pagerPictures.length; i++) {
                filePath = picsDir.getPath() + "/" + picsDir.list()[i];
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(filePath, options);
                options.inSampleSize = options.outWidth / getWindow().getDecorView().getWidth();
                options.inJustDecodeBounds = false;
                pagerPictures[i] = BitmapFactory.decodeFile(filePath, options);
            }

            pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
                @Override
                public Fragment getItem(final int i) {
                    return new Fragment() {
                        @Override
                        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                            View pageView = inflater.inflate(R.layout.viewpager_page, null);
                            ImageView image = (ImageView) pageView.findViewById(R.id.pager_image);
                            image.setImageBitmap(pagerPictures[i]);
                            return pageView;
                        }
                    };
                }

                @Override
                public int getCount() {
                    return pagerPictures.length;
                }
            });

            final LinearLayout pagerIndicator = (LinearLayout) detailView.findViewById(R.id.details_pager_indicator);
            pagerIndicator.removeAllViews();
            for (int i = 0; i < pagerPictures.length; i++) {
                ImageView indicator = new ImageView(this);
                indicator.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                if (i == 0)
                    indicator.setImageDrawable(getResources().getDrawable(R.drawable.dot_active_photo));
                else
                    indicator.setImageDrawable(getResources().getDrawable(R.drawable.dot_next_photo));
                pagerIndicator.addView(indicator);
            }

            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                private int current = 0;

                @Override
                public void onPageScrolled(int i, float v, int i1) {
                }

                @Override
                public void onPageSelected(int i) {
                    ((ImageView) pagerIndicator.getChildAt(current))
                            .setImageDrawable(getResources().getDrawable(R.drawable.dot_next_photo));
                    ((ImageView) pagerIndicator.getChildAt(i))
                            .setImageDrawable(getResources().getDrawable(R.drawable.dot_active_photo));
                    current = i;
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                }
            });
        }
    }

    private void setHandleDistance() {
        ((TextView) findViewById(R.id.handle_distance)).setText(String.format("%.2f km",
                (float) (additionalDistanceLeft > 0 ? additionalDistanceLeft : distanceLeft) / 1000));
    }

    private void setHandleDuration() {
        int time = additionalDurationLeft > 0 ? additionalDurationLeft : durationLeft;
        int m = (time / 60) % 60;
        int h = time / 3600;
        ((TextView) findViewById(R.id.handle_duration)).setText(String.format("%02dh %02dm", h, m));
    }

    private void decreaseDistances(int distanceTraversed) {
        if (additionalDistanceLeft > 0) {
            if ((additionalDistanceLeft -= distanceTraversed) < 0) {
                distanceLeft += additionalDistanceLeft;
                additionalDistanceLeft = 0;
            }
        } else {
            distanceLeft -= distanceTraversed;
        }
    }

    private void decreaseDurations(int durationTraversed) {
        if (additionalDurationLeft > 0) {
            if ((additionalDurationLeft -= durationTraversed) < 0) {
                durationLeft += additionalDurationLeft;
                additionalDurationLeft = 0;
            }
        } else {
            durationLeft -= durationTraversed;
        }
    }

    private void setProgressView(int progress) {
        ((TextView) findViewById(R.id.progressbar_caption)).setText(getString(R.string.progress_caption, progress));
        ((ProgressBar) findViewById(R.id.progressbar)).setProgress(progress);
    }

    private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    if (internetDialog != null && internetDialog.isShowing())
                        internetDialog.dismiss();
                }
            }
        }
    };

}