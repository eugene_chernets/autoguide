package com.audiocarguide.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.audiocarguide.R;
import com.audiocarguide.data.Tour;
import com.audiocarguide.fragments.ToursListFragment;
import com.audiocarguide.utils.FilterListener;

import java.util.ArrayList;


/**
 * Created by evgeniych on 25.12.2014.
 */
public class ACGToursListAdapter extends BaseExpandableListAdapter implements View.OnClickListener, FilterListener {

    private final String TAG = ACGToursListAdapter.class.getSimpleName();

    private Context context;
    private ExpandableListView toursListView;
    ToursListFragment fragment;
    private LinearLayout[] groupRoots;
    private Drawable selectedBackground, transparentBackground;
    private int selectedGroup;
    private ArrayList<Tour> allTours;
    private ArrayList<Tour> visibleTours;

    public ACGToursListAdapter(Context context, ExpandableListView toursListView, ToursListFragment fragment, ArrayList<Tour> tours) {
        this.context = context;
        this.toursListView = toursListView;
        this.fragment = fragment;
        this.allTours = tours;
        this.visibleTours = (ArrayList<Tour>)allTours.clone();
        groupRoots = new LinearLayout[getGroupCount()];
        selectedBackground = context.getResources().getDrawable(R.drawable.tour_group_selected);
        transparentBackground = new ColorDrawable(android.R.color.transparent);
        selectedGroup = 0;
    }

    @Override
    public int getGroupCount() {
        return visibleTours.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return visibleTours.get(groupPosition).getStages().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (visibleTours.get(groupPosition).isOffer())
            return createOfferView(groupPosition);
        else
            return createTourView(groupPosition, isExpanded);
    }

    private View createTourView(int groupPosition, boolean isExpanded) {
        View view = LayoutInflater.from(context).inflate(R.layout.tour_list_group_tour, null);
        ImageView indicator = (ImageView)view.findViewById(R.id.expand_group_indicator);
        indicator.setTag(groupPosition);
        indicator.setOnClickListener(this);
        groupRoots[groupPosition] = (LinearLayout) view.findViewById(R.id.expandable_group_root);
        if (selectedGroup == groupPosition) {
            groupRoots[groupPosition].setBackgroundDrawable(selectedBackground);
            groupRoots[groupPosition].findViewById(R.id.btn_action).setEnabled(true);
        } else {
            groupRoots[groupPosition].setBackgroundDrawable(transparentBackground);
            groupRoots[groupPosition].findViewById(R.id.btn_action).setEnabled(false);
        }
        if (isExpanded)
            indicator.setImageDrawable(context.getResources().getDrawable(R.drawable.btn_route_details_up));
        else
            indicator.setImageDrawable(context.getResources().getDrawable(R.drawable.btn_route_details_down));

        ((TextView)view.findViewById(R.id.tour_name)).setText(visibleTours.get(groupPosition).getTourName());
        ((TextView)view.findViewById(R.id.tour_distance))
                .setText(visibleTours.get(groupPosition).getDistanceString());
        ((TextView)view.findViewById(R.id.tour_duration))
                .setText(visibleTours.get(groupPosition).getDurationString());
        view.findViewById(R.id.btn_action).setTag(visibleTours.get(groupPosition).getPath());
        return view;
    }

    private View createOfferView(int groupPosition) {
        View view = LayoutInflater.from(context).inflate(R.layout.tour_list_group_offer, null);
        groupRoots[groupPosition] = (LinearLayout) view.findViewById(R.id.expandable_group_root);
        if (selectedGroup == groupPosition) {
            groupRoots[groupPosition].setBackgroundDrawable(selectedBackground);
            groupRoots[groupPosition].findViewById(R.id.btn_action).setEnabled(true);
        } else {
            groupRoots[groupPosition].setBackgroundDrawable(transparentBackground);
            groupRoots[groupPosition].findViewById(R.id.btn_action).setEnabled(false);
        }
        ((TextView)view.findViewById(R.id.tour_name)).setText(visibleTours.get(groupPosition).getTourName());
        ((TextView)view.findViewById(R.id.tour_distance))
                .setText(visibleTours.get(groupPosition).getDistanceString());
        ((TextView)view.findViewById(R.id.tour_duration))
                .setText(visibleTours.get(groupPosition).getDurationString());

        view.findViewById(R.id.btn_action).setTag(groupPosition);
        view.findViewById(R.id.btn_action).setOnClickListener(fragment);
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.tour_list_group_item, null);
        ((TextView)view.findViewById(R.id.tour_stage_number)).setText("" + (childPosition + 1));
        ((TextView)view.findViewById(R.id.tour_name)).setText(visibleTours.get(groupPosition).getTourName());
        ((TextView)view.findViewById(R.id.tour_stage_name))
                .setText(visibleTours.get(groupPosition).getStages().get(childPosition).getName());

        float relativeDistance = visibleTours.get(groupPosition).getAdditionalDistance();
        for (int i = 0; i < childPosition; i++)
            relativeDistance += visibleTours.get(groupPosition).getStages().get(i).getDistance();
        ((TextView)view.findViewById(R.id.tour_stage_start_distance))
                .setText(String.format("%.1f", relativeDistance / 1000) + "km");

        ((TextView)view.findViewById(R.id.tour_stage_distance))
                .setText(visibleTours.get(groupPosition).getStages().get(childPosition).getDistanceString() + "\nkm");

        if (isLastChild)
            view.findViewById(R.id.connecting_line).setVisibility(View.GONE);
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void setGroupSelected(int groupPosition) {
        groupRoots[selectedGroup].setBackgroundDrawable(transparentBackground);
        groupRoots[selectedGroup].findViewById(R.id.btn_action).setEnabled(false);
        selectedGroup = groupPosition;
        groupRoots[selectedGroup].setBackgroundDrawable(selectedBackground);
        groupRoots[selectedGroup].findViewById(R.id.btn_action).setEnabled(true);
    }

    public int getSelectedGroup() {
        return selectedGroup;
    }

    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        if (position >=0 && position < getGroupCount()) {
            if(toursListView.isGroupExpanded(position))
                toursListView.collapseGroup(position);
            else
                toursListView.expandGroup(position);
        }
    }

    public void addFiltered(String name) {
        boolean wasEmpty = visibleTours.size() == 0;
        for (int i = 0; i < allTours.size(); i++) {
            if (name.equals(allTours.get(i).getCountry().getRegions().get(0).getName())) {
                visibleTours.add(allTours.get(i));
            }
        }
        if (wasEmpty && allTours.size() > 0)
            fragment.showTourListDrawer();
        this.notifyDataSetChanged();
    }

    public void removeFiltered(String name) {
        for (int i = 0; i < visibleTours.size(); i++) {
            String regname = visibleTours.get(i).getCountry().getRegions().get(0).getName();
            if (name.equals(regname)) {
                visibleTours.remove(i--);
            }
        }
        this.notifyDataSetChanged();
        if (visibleTours.size() == 0)
            fragment.hideTourListDrawer();
    }
}
