package com.audiocarguide.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.audiocarguide.R;
import com.audiocarguide.data.TourStage;

import java.util.ArrayList;

/**
 * Created by evgeniych on 25.12.2014.
 */
public class ACGListAdapter implements ListAdapter {
    private Context context;
    private ArrayList<TourStage> checkpoints;

    public ACGListAdapter(Context context, ArrayList<TourStage> checkpoints) {
        this.context = context;
        this.checkpoints = checkpoints;
    }
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return checkpoints.size();
    }

    @Override
    public Object getItem(int position) {
        return checkpoints.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.route_point_list_item, null);
        ((TextView)view.findViewById(R.id.tour_point_number)).setText((position + 1) + "");
        ((TextView)view.findViewById(R.id.tour_point_name)).setText(checkpoints.get(position).getName());

        ((TextView)view.findViewById(R.id.tour_point_duration)).setText(checkpoints.get(position).getDurationString());
        ((TextView)view.findViewById(R.id.tour_point_distance)).setText(checkpoints.get(position).getDistanceString());

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
