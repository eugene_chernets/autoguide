package com.audiocarguide.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.audiocarguide.R;

/**
 * Created by evgeniych on 05.01.2015.
 */
public class ACGDrawerListAdapter implements ListAdapter {
    private Context context;
    private String[] items_names;
    private TypedArray items_icons, items_icons_selected;
    private ImageView[] images;
    private RelativeLayout[] itemsRoots;
    private int selectedItem;
    private Drawable selectedBackground, transparentBackground;

    public ACGDrawerListAdapter(Context context, int selectedItem) {
        this.context = context;
        this.selectedItem = selectedItem;
        items_names = context.getResources().getStringArray(R.array.drawer_menu_names);
        items_icons = context.getResources().obtainTypedArray(R.array.drawer_menu_icons);
        items_icons_selected = context.getResources().obtainTypedArray(R.array.drawer_menu_icons_selected);
        images = new ImageView[items_names.length];
        itemsRoots = new RelativeLayout[items_names.length];
        selectedBackground = context.getResources().getDrawable(R.drawable.drawer_menu_item_selected);
        transparentBackground = new ColorDrawable(android.R.color.transparent);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return items_names.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.drawer_menu_item, null);
        ((TextView)view.findViewById(R.id.drawer_menu_item_text)).setText(items_names[position]);
        itemsRoots[position] = (RelativeLayout) view.findViewById(R.id.drawer_menu_item_root);
        images[position] = (ImageView) view.findViewById(R.id.drawer_menu_item_icon);
        images[position].setBackgroundDrawable(items_icons.getDrawable(position));
        if (selectedItem == position) {
            images[selectedItem].setBackgroundDrawable(items_icons_selected.getDrawable(selectedItem));
            itemsRoots[selectedItem].setBackgroundDrawable(selectedBackground);
        }
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    public void setIconSelected(int position) {
        images[selectedItem].setBackgroundDrawable(items_icons.getDrawable(selectedItem));
        itemsRoots[selectedItem].setBackgroundDrawable(transparentBackground);
        selectedItem = position;
        images[selectedItem].setBackgroundDrawable(items_icons_selected.getDrawable(selectedItem));
        itemsRoots[selectedItem].setBackgroundDrawable(selectedBackground);
    }
}
