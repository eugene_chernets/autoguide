package com.audiocarguide.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.audiocarguide.R;
import com.audiocarguide.custom.ACGCheckBox;
import com.audiocarguide.data.Country;
import com.audiocarguide.data.Region;
import com.audiocarguide.data.Tour;
import com.audiocarguide.utils.FilterListener;

import java.util.ArrayList;

/**
 * Created by evgeniych on 05.01.2015.
 */
public class ACGFilterListAdapter extends BaseExpandableListAdapter implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private Context context;
    private FilterListener filterListener;
    private ArrayList<Country> countries = new ArrayList<Country>();

    public ACGFilterListAdapter(Context context, ArrayList<Tour> tours, FilterListener filterListener) {
        this.context = context;
        this.filterListener = filterListener;
        for (Tour tour : tours) {
            Country country = tour.getCountry();
            Region region = country.getRegions().get(0);
            if (countries.contains(country)) {
                if (!countries.get(countries.indexOf(country)).getRegions().contains(region))
                    countries.get(countries.indexOf(country)).getRegions().add(region);
            } else {
                countries.add(country);
            }
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return countries.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return countries.get(groupPosition).getRegions().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.filter_list_group, null);
        ((TextView)view.findViewById(R.id.filter_group_text)).setText(countries.get(groupPosition).getName());

        ExpandableListView expandableListView = (ExpandableListView) parent;
        expandableListView.expandGroup(groupPosition);

        view.setOnClickListener(this);

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.filter_list_group_item, null);
        ACGCheckBox acgCheckBox = (ACGCheckBox) view.findViewById(R.id.filter_item);
        acgCheckBox.setText(countries.get(groupPosition).getRegions().get(childPosition).getName());
        acgCheckBox.setOnCheckedChangeListener(this);
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onClick(View v) {
        //todo
        Log.d("filter", "stubbed onClick");
        //do nothing just to disable group onclick behavior
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d("filter", buttonView.getText().toString() + " isChecked " + buttonView.isChecked());
        if (isChecked)
            filterListener.addFiltered(buttonView.getText().toString());
        else
            filterListener.removeFiltered(buttonView.getText().toString());
    }
}
