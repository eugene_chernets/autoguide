package com.audiocarguide.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.audiocarguide.R;


/**
 * Created by evgeniych on 25.12.2014.
 */
public class ACGOfferDetailsListAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    private final String TAG = ACGOfferDetailsListAdapter.class.getSimpleName();

    private Context context;
    private String[] details;
    private ExpandableListView detailsListView;

    public ACGOfferDetailsListAdapter(Context context, ExpandableListView detailsListView, String[] details) {
        this.context = context;
        this.detailsListView = detailsListView;
        this.details = details;
    }

    @Override
    public int getGroupCount() {
        return details.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.offer_list_group, null);
        ImageView indicator = (ImageView)view.findViewById(R.id.offer_list_group_indicator);
        indicator.setTag(groupPosition);
        indicator.setOnClickListener(this);
        if (isExpanded)
            indicator.setImageDrawable(context.getResources().getDrawable(R.drawable.offer_collapse));
        else
            indicator.setImageDrawable(context.getResources().getDrawable(R.drawable.offer_expand));

        ((TextView)view.findViewById(R.id.offer_list_group_title))
                .setText(context.getResources().getStringArray(R.array.offer_details_titles)[groupPosition]);

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.offer_list_group_item, null);
        ((TextView)view.findViewById(R.id.offer_list_group_item_text)).setText(details[groupPosition]);
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        if (position >=0 && position < getGroupCount()) {
            if(detailsListView.isGroupExpanded(position))
                detailsListView.collapseGroup(position);
            else
                detailsListView.expandGroup(position);
        }
    }

}
